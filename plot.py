"""
Semplice esempio per la generazione di mappe PNG senza bordi - quindi
facilmente geoereferenziabili. Lo script presuppone di avere in ingresso
le 73 scadenze di precipitazione (analisi + 72 previsioni).

Author: Emanuele Di Giacomo <edigiacomo@arpae.it>
License: GPLv2+
"""

from argparse import ArgumentParser
import json
import itertools
import math
import os
from datetime import datetime, timedelta


def deg2num(lon_deg, lat_deg, zoom):
    lat_rad = math.radians(lat_deg)
    n = 2.0 ** zoom
    xtile = int((lon_deg + 180.0) / 360.0 * n)
    ytile = int((1.0 - math.asinh(math.tan(lat_rad)) / math.pi) / 2.0 * n)
    return (xtile, ytile)


def num2deg(xtile, ytile, zoom):
    n = 2.0 ** zoom
    lon_deg = xtile / n * 360.0 - 180.0
    lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n)))
    lat_deg = math.degrees(lat_rad)
    return (lon_deg, lat_deg)


# Bounding box e dimensione del file devono essere proporzionali
# bbox = [7.0, 43., 14., 46.5]
width = 256
height = 256
crs_name = "EPSG:3857"
output_format = "png"

# min_x, min_y, max_x, max_y = bbox
width_cm = width / 40.
height_cm = height / 40.


def do_magics_plot(bbox, grib_params, contour_params, output_name):
    from Magics import macro


    print(f"Plotting {output_name}")
    min_x, min_y, max_x, max_y = bbox

    grib = macro.mgrib(grib_params)
    mmap = macro.mmap(
        subpage_map_projection=crs_name,
        subpage_lower_left_latitude=min_y,
        subpage_lower_left_longitude=min_x,
        subpage_upper_right_latitude=max_y,
        subpage_upper_right_longitude=max_x,
        subpage_frame='off',
        page_x_length=width_cm,
        page_y_length=height_cm,
        super_page_x_length=width_cm,
        super_page_y_length=height_cm,
        subpage_x_length=width_cm,
        subpage_y_length=height_cm,
        subpage_x_position=0.,
        subpage_y_position=0.,
        output_width=width,
        page_frame='off',
        skinny_mode="on",
        page_id_line='off',
    )
    output = macro.output(
        output_formats=[output_format],
        output_name_first_page_number='off',
        output_cairo_transparent_background=True,
        output_width=width,
        output_name=output_name,
    )

    contour = macro.mcont(**contour_params)
    macro.silent()
    os.environ["MAGPLUS_QUIET"] = "yes"
    macro.plot(output, mmap, grib, contour)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--zoom", type=int)
    parser.add_argument("--step", type=int)
    parser.add_argument("product", choices=["tp", "tcc"])
    parser.add_argument("emission_time",
                        type=lambda s: datetime.strptime(s, "%Y%m%d%H%M"),
                        help="Reference time (dataDate + dataTime)")
    parser.add_argument("inputfile")
    args = parser.parse_args()

    geobounds = [-2.0, 33.0, 27.0, 50.0]

    if args.zoom is not None:
        zooms = [args.zoom]
    else:
        zooms = range(3, 6)

    if args.step is not None:
        steps = [args.step]
    else:
        steps = range(0, 73)

    from multiprocessing import Pool
    pool = Pool()

    for z in zooms:
        tilesbounds = [
            deg2num(geobounds[0], geobounds[1], z),
            deg2num(geobounds[2], geobounds[3], z),
        ]

        for x, y in itertools.product(
            range(tilesbounds[0][0], tilesbounds[1][0] + 1),
            range(tilesbounds[1][1], tilesbounds[0][1] + 1),
        ):
            nw = num2deg(x, y, z)
            se = num2deg(x + 1, y + 1, z)
            bbox = [nw[0], se[1], se[0], nw[1]]
            min_x, min_y, max_x, max_y = bbox

            # Si suppone che il GRIB contenga solo le 73 scadenze in ordine
            for step in steps:
                index = step + 1

                grib_params = {
                    "grib_input_file_name": args.inputfile,
                    "grib_field_position": index,
                }

                with open(f"{args.product}.json") as fp:
                    contour_params = json.load(fp)

                validity_time = args.emission_time + timedelta(hours=step)
                output_name = (
                    f"{args.product}/{validity_time:%Y%m%d%H}"
                    f"/{z}/{x}/{y}"
                )
                basedir = os.path.dirname(output_name)
                os.makedirs(basedir, exist_ok=True)

                pool.apply_async(do_magics_plot, kwds={
                        "bbox": bbox,
                        "grib_params": grib_params,
                        "contour_params": contour_params,
                        "output_name": output_name,
                })

    pool.close()
    pool.join()
