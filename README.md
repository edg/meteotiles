# Meteotiles

Semplice prototipo per la generazione e la visualizzazione di tiles generati con Magics.


## Utilizzo:


Scarico le 73 scadenze di precipitazione

```
arki-query \
    --sort=hour:timerange,product \
    --data \
    'reftime:=today 00:00; product:GRIB1,,002,061; level:GRIB1,1;' \
    http://arkiope.metarpa:8090/dataset/cosmo_5M_ita > tp.grib
```

Scarico le 73 scadenze di copertura nuvolosa

```
arki-query \
    --sort=hour:timerange,product \
    --data \
    'product: GRIB1,,2,71; level: GRIB1,1; reftime:=today 00:00' \
    http://arkiope.metarpa:8090/dataset/cosmo_5M_ita > tcc.grib
```

Genero le mappe

```
python3 -u plot.py tp 202002250000 tp.grib
python3 -u plot.py tcc 202002250000 tcc.grib
```

È anche possibile generare un solo zoom e step alla volta

```
for z in {3..5}
do
    for s in {0..72}
    do
        python3 plot.py --zoom $z --step $s tcc 202002250000 tcc.grib
    done
done
```

Le mappe sono generate nella directory corrente con pattern `{product}/{YYYYmmddHH}/{z}/{x}/{y}.png` (la data è quella
di emissione).

Lo stile usato per il contouring del prodotto è caricato dal file `{product}.json`.

Apro `index.html` per visualizzare la mappa di precipitazione:

```
python3 -mhttp.server
firefox http://localhost:8000
```

![Screenshot](./screenshot.gif)


## Limiti

Per ora è un semplice proof of concept, quindi i limiti sono parecchi, in particolare:

- Lo script si aspetta un GRIB con le sole 73 scadenze, in ordine
- Numerosi parametri sono harcoded nello script Python e nel client Javascript
  - Gli zoom generati
  - Il bounding box della mappa
  - Le scadenze, lo step e l'istante di emissione
  - I prodotti disponibili (solo lato Javascript)

Una possibile soluzione per i parametri hardcoded è di passarli allo script e questi deve generare un file JSON da
rendere disponibile nel path radice per il client Javascript, che può così costruire i vari layer in modo dinamico.
